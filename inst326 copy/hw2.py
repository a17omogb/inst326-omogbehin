import random
import sys
from argparse import ArgumentParser
def min_payment(principal, annual_ir, term = 30, per_year = 12):
    payment = (principal*(annual_ir/per_year)*(1 + (annual_ir/per_year))**(term * per_year))
    return payment


def interest_due(balance, annual_ir, per_year = 12):
    c=annual_ir/per_year
    interest=balance*c
    return interest

def remaining_payments(balance, annual_ir, payment_amount, per_year = 12):
    counter = 0
    while balance>0:
        pay = balance - interest_due(balance, annual_ir, per_year)
        balance = balance - pay
        counter += 1
    return counter

def main(mortgage, annual_ir, term = 30, per_year = 12, target_payment = None):
    min = min_payment(mortgage, annual_ir, term, per_year)
    print('The minimum payment is: ', min)
    if target_payment is None:
        target_payment = min
    if target_payment < min:
        print('Your target payment is less than the minimum payment for this mortgage')
    else:
        num=remaining_payments(mortgage,annual_ir,target_payment,per_year)
        print('if you make payments of $', target_payment, ', you will pay off the mortgage in ', num, ' payments.')
        
    

def parse_args(arglist):
    parser = ArgumentParser()
    parser.add_argument("principal", type= float)
    parser.add_argument("annual_ir", type= float)
    parser.add_argument("-term", type=int, default=30,
                        help="the term of the mortgage in years")
    parser.add_argument("-per_year", type=int, default = 12,
                        help="the number of payments in a year")
    parser.add_argument("-target_payment", type=float,
                        help = "the target payment amount")
    args = parser.parse_args(arglist)
    return args
    
 
if __name__=='__main__':
    variable=parse_args(sys.argv[1:])
    main(variable.principal, variable.annual_ir, variable.term, variable.per_year, variable.target_payment)
    
               
        