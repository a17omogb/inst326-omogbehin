"""A class that loads a maze and finds the shortest path to 
complete the maze
"""
import sys
class Maze:
    """class attributes:
    
    maze: a list of strings where each string represents a row in the maze.
    
    start: a tuple consisting of two integers, an x coordinate and a 
    y coordinate, respectively, representing the start of the maze. 
    The y coordinate corresponds to an index in the maze attribute; 
    the x coordinate corresponds to an index in the y-th string in the 
    maze attribute.
    
    end: : a tuple consisting of two integers, an x coordinate and a 
    y coordinate, respectively, representing the end of the maze. 
    The y coordinate corresponds to an index in the maze attribute; 
    the x coordinate corresponds to an index in the y-th string in the 
    maze attribute.
    """
    
    
    def __init__(self,path):
        """Calls the load() method to read the specified file.
        
        Args: 
            path(str): A path to a plain text file in UTF-8 encoding
            containing a maze.
        """ 
        self.load(path)
        
    def load(self,path):
        """loads the file into the class and its attributes
        
        Args:
            path(str):  A path to a plain text file in UTF-8 encoding
            containing a maze.
        
        Side Effects:
        - Calls the find_opening() method to find the end
        of the maze and sets the end attribute.
        
        - Calls the find_opening() method to find the start of
        the maze and sets the start attribute.
        
        - Calls the find_opening() method to find the end 
        of the maze and sets the end attribute.
        
        
        """
        self.maze = []
        self.start = ()
        self.end = ()
        with open(path,'r',encoding='utf-8') as f:
            for line in f:
                line = line.strip("\n")
                self.maze.append(line)
        self.start = self.find_opening(0)
        self.end = self.find_opening(-1)
        
    def find_opening(self,opening_index):
        """finds the openings in the maze
        
        Args: 
        opening_index(int): The position to at which to look 
        for an opening. Should be 0 for the start of the
        string or -1 for the end of the string.
        
        Returns:
        opening(tuple):  Returns the x, y coordinates of 
        the opening (the x coordinate will be 0 or the 
        length of the string minus 1; the y coordinate 
        will be the index of the string containing the 
        opening)
        
        
        
        """
        
        
        enumeration = list(enumerate(self.maze))
        
        
        for i,j in enumeration:
            
            if j[opening_index]== ' ':
                if opening_index == 0:
                    return (opening_index,i)
                elif opening_index == -1:
                    return (len(j)-1,i)
            

                
    def solve(self):
        """
        solves the maze by returning the coordinates from
        the start position to the end position.
        
        Args: none
        
        Returns:
        paths(list): Finds a path of (x, y) coordinates through the maze 
        specified in the object's maze attribute. Returns this path as a
        list of tuples. 
        
        """
        
        paths = [[self.start]]
        ##print(paths)
        visited = [self.start]
        
        while len(paths)>0:
            current_path = paths.pop()
            current_pos = current_path[-1]
            if current_pos == self.end:
                return current_path
            for i,j in [(-1,0,),(0,1),(1,0),(0,-1)]:
                x = current_pos[0] + j
                y = current_pos[1] + i
                if (self.maze[y][x] == ' ' 
                    and (current_pos[0]+j,current_pos[1]+i) not in visited):
                    visited.append((current_pos[0]+j,current_pos[1]+i))
                    path_copy = current_path.copy()
                    path_copy.append((current_pos[0]+j,current_pos[1]+i))
                    paths.append(path_copy)
        return paths
                    
        
       
    

def main(path):
    """creates an instance of the maze class. Solves the maze and prints
    the output to the console.
    
    Args:
    path(str): A path to a plain text file in UTF-8 encoding
    containing a maze.

    """
    maze = Maze(path)
    print(maze.solve())
    
if __name__ == '__main__':
    main(sys.argv[1])
        