"""Function Description: This function will calculate the amount of taxes to be taken out of a persons net income.

    Arguments: This function takes an integer called 'income', which represents a person's net income. 
            The second argument is a boolean called 'joint' which represents whether a person is filing 
            joint returns or as single. If 'joint' is equal to true, then the function proceeds to calculate the amount of tax
            for a person filing as joint or married. If 'joint' is equal to false, then the function proceeds to calculate the
            amount of tax for a person filing as single or married filing seperately.
            
    Returns: This function returns a float that represents the USD amount of money taken out of a person's 
            net income for taxes.
"""

def estimate_taxes(income,joint=False):
    if joint==True:
        if income > 0 and income <= 1000:
            tax = 0.02 * income
        if income > 1000 and income <= 2000:
            tax = 20 + 0.03 * (income - 1000)
        if income > 2000 and income <= 3000:
            tax = 50 + 0.04 * (income - 2000)
        if income > 3000 and income <= 150000:
            tax = 90 + 0.0475 * (income - 3000)
        if income > 150000 and income <= 175000:
            tax = 7072.50 + 0.05 * (income - 150000)
        if income > 175000 and income <= 225000:
            tax = 8322.50 + 0.0525 * (income - 175000)
        if income > 225000 and income <= 300000:
            tax = 10947.50 + 0.0550 * (income - 225000)
        if income > 300000:
            tax = 15072.50 + 0.0575 * (income - 300000)
        return(round(tax,2))
    else:
        if income > 0 and income <= 1000:
            tax = 0.02 * income
        if income > 1000 and income <= 2000:
            tax = 20 + 0.03 * (income - 1000)
        if income > 2000 and income <= 3000:
            tax = 50 + 0.04 * (income - 2000)
        if income > 3000 and income <= 100000:
            tax = 90 + 0.0475 * (income - 3000)
        if income > 100000 and income <= 125000:
            tax = 4697.50 + 0.05 * (income - 100000)
        if income > 125000 and income <= 150000:
            tax = 5947.50 + 0.0525 * (income - 125000)
        if income > 150000 and income <= 250000:
            tax = 7260.00 + 0.0550 * (income - 150000)
        if income > 250000:
            tax = 12760.00+ 0.0575 * (income - 300000)
        return(round(tax,2))
  


if __name__ == '__main__' :
    user_income= int(input ("please enter your income: "))
    filing= input ("please enter 'j' for joint filing or 's' for single filing: ") 
    if filing=='j':
        print(estimate_taxes(user_income,joint=True))
    elif filing=='s':
        print(estimate_taxes(user_income,joint=False))  
  
