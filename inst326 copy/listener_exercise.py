"""adds or removes artists from a list in a file"""
class Listener:
    """
    Attributes:
    name(str): name of artist
    """
    def __init__(self,name):
        self.name=name
        self.artists=list()
        
    def add_artist(self,artist):
        """Args:
        artist- type: string; name of artist to be added
        
        Side Effects:
        adds artist to list artists
        """
        self.artists=self.artists.append(artist)
        
    def remove_artist(self,artist):
        """Args:
        artist- type: string; name of artist to be removed
        
        Side Effects:
        removes artist to list artists
        """
        if artist in self.artists:
            self.artists=self.artists.remove(artist)
            
    def load_artists(self,path):
        """Args:
        path- type: string; path of the file to be opened
        
        Side Effects:
        adds artists to the list in the file
        """
        with open(path) as f:
            f = f.readlines()
            for line in f:
                line.strip()
                self.add_artist(line)
                
    def save_artists(self,path):
          """Args:
        path- type: string; path of the file to be opened
        
        Side Effects:
        saves all artists in the file
        """
        with open(path,'w',encoding='utf-8') as f:
            for artist in self.artists:
                f.write("{}\n".format(artist))
            
                
                
        
        