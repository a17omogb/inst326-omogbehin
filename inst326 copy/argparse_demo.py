from argparse import ArgumentParser
import sys

def main(hours, wage=10.10, overtime=None):
    if overtime is None:
        overtime = 1
    wages = ((hours % 41)* wage )
    if hours > 40:
            wages += (hours - 40) * wage * overtime
    print("You earned ${} this week".format(wages))
def parse_args(arglist):
    parser = ArgumentParser()
    parser.add_argument("-wage", type=float, default=10.10,
                        help="your hourly wage")
    parser.add_argument("-overtime", type=float,
                        help="how much to mulitply your wage if you work more than 40 hours per week")
    parser.add_argument("hours", type=int,
                        help = "number of hours")
    args = parser.parse_args(arglist)
    return args
    
if __name__=='__main__':
    args = parse_args(sys.argv[1:])
    main(args.hours,args.wage,args.overtime)