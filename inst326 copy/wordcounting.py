"""reads one or more text files and counts the words in those files"""

from pathlib import Path
import sys
import re

def tokenize(s):
    """return a list of words in s.
    
    Args: 
    s (str): a string to tokenize
    
    Returns:
     (list of str): the words found in s
     """
    return [w.lower() for w in re.findall(r"[\w'-]+",s)]
class WordCount:
    def __init__(self):
        """initializes the count dictionary"""
        self.counts={}
    
    def read(self,path):
        """Opens, reads, and tokenizes words from a file
        
        Args: path(str); a path to a file to be opened"""
        with open(path,'r',encoding='utf-8') as f:
            for line in f:
                line = line.strip()
                tokens = tokenize(line)
                for token in tokens:
                    self.counts[token]=1
                    if token in self.counts:
                        self.counts[token]+=1   
    def most_common(self,num_words):
        """Returns as many words as specified in the argument, 
        in order of decreasing frequency from the most 
        frequent word
        
        Args: num_words (int); number of words to return
        
        Returns:
        (list) returns as many words as specified in the argument, 
        in order of decreasing frequency from the most frequent word
        """
        sorted_list = sorted(self.counts.items(), key=lambda x: x[1])
        ##needed to use the internet to find out how to sort a dictionary by values. Not sure if this is valid
        
        return sorted_list[0:num_words]
    
    def n_instances(self,inst_num):
        """Returns a list of all words whose count is 
        exactly the specified integer.
        
        Args:
        inst_num (int); Returns a list of all words whose count 
        is exactly the specified integer
        
        Returns:
        (list); a list of all words whose count is the same as inst_num
        """
        self.inst_list=[]
        for word in self.counts:
            if self.counts[word] == inst_num:
                self.inst_list.append(word)
        return self.inst_list

def main(path,num_of_words,inst_num):
        """instantiates a WordCount object and count the words in the specified file.
        
        Args:
        path (str): a file path
        num_of_words (int): number of most common words to print
        inst_num (int): number of specific instances for a word
        """
        wordcount = WordCount()
        wordcount.read(path)
        print("The {} most common words were: ".format(num_of_words),wordcount.most_common(num_of_words))
        print("The following words occured {} times".format(inst_num),wordcount.n_instances(inst_num))
        
if __name__=='__main__':
    
    main(sys.argv[1],sys.argv[2],sys.argv[3])
    