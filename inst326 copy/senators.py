"""represent US senators and do calculations on the..."""
import sys
class Senator:
        """Represent a US senator
        
        Attributes:
        name(str): the senator's name
        party(str): the senator's party
        state(str): the state the senator represents
        rank(int): the senator's rank of seniority
        """
        def __init__(self,name,party,state,rank):
            self.name=name
            self.party=party
            self.state=state
            self.rank=rank
def read_senators(path):
        """Read senators from a file and return a list of senator objects.
        
        Args:
            path(str): path to a TSV file containing senators
        
        Returns:
            list of Senator: a list of the senators in the file.
        """
        senators=[]
        with open(path,'r',encoding="utf-8") as f:
            f.readline()
            for line in f:
                line = line.strip()
                if not line:
                    continue
                senator, party, state, rank = line.split("\t")
                senators.append(Senator(senator, party, state, rank))
        return senators
    
def main(path):
        senators = read_senators(path)
        names = [senator.name for senator in senators]
        names.sort(key=lambda name: name.split()[1])
        print(names)
        
if __name__=="__main__":
        main(sys.argv[1])