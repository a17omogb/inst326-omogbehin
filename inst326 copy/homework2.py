"""This script contains functions that help one calculate payment information about their mortgage"""

import random
import sys
from argparse import ArgumentParser

def min_payment(principal, annual_ir, term = 30, per_year = 12):
    
    """This function computes and returns the minimum mortgage payment using this formula:
       
        p=(Lc(1+c)^n)/((1+c)^n-1)
        where p is the minimum payment amount,
        L is the principal amount,
        c is the interest rate per payment (the annual interest rate divided by the number of payments per year),
        and n is the total number of payments (the term of the mortgage in years times the number of payments per year)
        
        Args:
        principal- type: positive integer or float; the total amount of the mortgage
        annual_ir- type: float; the annual interest rate. Should be a float between 0 and 1
        term- type: int; default value: 30; the term of the mortgage in years
        per_year: type: int; default value: 12; the number of payments per year
        
        
        Returns:
        returns the p value from the formula for future use.
    """
    payment =(principal*(annual_ir/per_year)*(1 + (annual_ir/per_year))**(term * per_year))/((1+(annual_ir/per_year))**(term * per_year)-1)
    return payment


def interest_due(balance, annual_ir, per_year = 12):
    """This function computes and returns the amount of interest due in the next payment according to the following formula:
       
        i=bc
        where i is the amount of interest due in the next payment,
        b is the balance of the mortgage
        and c is the interest rate per payment (the annual interest rate divided by the number of payments per year)
        
        Args:
        balance- type: positive integer or float; the balance of the mortgage 
        annual_ir- type: float; the annual interest rate; should be a float between 0 and 1
        per_year- type: int; default value: 12; the number of payments per year; should be a positive integer
        
        Returns:
        returns the amount of interest due for the next payment
        
    """
    c=annual_ir/per_year
    interest=balance*c
    return interest

def remaining_payments(balance, annual_ir, payment_amount, per_year = 12):
    """This function computes and returns the number of payments required to pay off the mortgage
    
    Args:
    balance- type: int or float; the balance of the mortgage; should be a positive integer or float
    annual_ir- type: float; the annual interest rate; should be a float between 0 and 1
    payment_amouunt- type: int or float; the payment amount; should not be less than the amount returned from min_payment();
    should be a positive integer or float
    per_year- type: int; default value: 12; the number of payments per year; should be a positive integer
    
    
    Returns:
    returns the number of payments required to pay off the mortgage
    """
    counter = 0
    while balance>0:
        counter += 1
        pay = interest_due(balance, annual_ir, per_year)
        balance = balance - (payment_amount-pay)
    return counter

def main(mortgage, annual_ir, term = 30, per_year = 12, target_payment = None):
    """This function uses previous funtions to display information to the user
    Args:
    mortgage- type: int or float; the total amount of the mortgage; should be a positive integer or float
    annual_ir- type: float; the annual interest rate; should be a float between 0 and 1
    term- type: int; default value: 30; the term of the mortgage, in years; should be a positive integer
    per_year- type: int; default value: 12; the number of payments per year; should be a positive integer
    target_payment- type: int or float; default value: None; the amount the user wishes to pay per payment;should be a positive integer or float, or None
    
    """
    min = min_payment(mortgage, annual_ir, term, per_year)
    print('The minimum payment is: ', min)
    if target_payment is None:
        target_payment = min
    if target_payment < min:
        print('Your target payment is less than the minimum payment for this mortgage')
    else:
        num=remaining_payments(mortgage,annual_ir,target_payment, per_year)
        
        print('if you make payments of $', target_payment, ', you will pay off the mortgage in ', num, ' payments.')
        
    

def parse_args(arglist):
    """This function collects arguments for previous functions from the command line
    Args:
    a list of strings containing the command line arguments to the program
    """
    parser = ArgumentParser()
    parser.add_argument("principal", type= float)
    parser.add_argument("annual_ir", type= float)
    parser.add_argument("-term", type=int, default=30,
                        help="the term of the mortgage in years")
    parser.add_argument("-per_year", type=int, default = 12,
                        help="the number of payments in a year")
    parser.add_argument("-target_payment", type=float,
                        help = "the target payment amount")
    args = parser.parse_args(arglist)
    return args
    
 
if __name__=='__main__':
    variable=parse_args(sys.argv[1:])
    main(variable.principal, variable.annual_ir, variable.term, variable.per_year, variable.target_payment)
    
               
        