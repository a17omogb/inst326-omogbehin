"""This script contains class Triangle, which has functions to calculate certain attributes of a triangle"""
import math
import sys
from argparse import ArgumentParser

class Triangle:
   
    def __init__(self, a, b, c):
        self.side_a=a
        self.side_b=b
        self.side_c=c
        if not self.is_valid():
            raise ValueError
        self.angle_c = self.law_of_cosines(a,b,c)
        self.angle_b = self.law_of_cosines(a,c,b)
        self.angle_a = self.law_of_cosines(c,b,a)
        self.area= self.calculate_area()
        

    def is_valid(self):
        """
        Determines whether a triangle in given sides a, b, and c can make a" 
        " closed triangle
        Args: (float) a, b, c - lengths representing the sides of a"
        " hypothetical triangle
        Returns: (boolean) True of False
        """
        if (self.side_a + self.side_b > self.side_c and self.side_a + 
            self.side_c > self.side_b and self.side_b + self.side_c > 
            self.side_a):
            return True
        else:
            return False
    

    def law_of_cosines(self,side_a,side_b,side_c):
        """
        Calculates the cosine of angle c by utilizing the law of cosines
        Args: (float) a, b, c - lengths representing the sides of a "
        "hypothetical triangle
        Raises: ValueError; if the three given sides of the given triangle "
        "cannot form a closed triangle
        Returns: (float) angle_deg; the measurement of angle c in degrees
        """
        cosine = ((self.side_a**2 + self.side_b**2 - self.side_c**2)/
                 (2*self.side_a*side_b))
        angle_deg = math.degrees(math.acos(cosine))
        return(angle_deg)


    def calculate_area(self):
        """
        Calculates the area of a triangle with the passed in side lengths
        Args: (float) a, b, c - lengths representing the sides of a hypothetical triangle
        Raises: ValueError; if the three given sides of the given triangle cannot form a closed triangle
        Returns: (float) area; Area of the given triangle after calculating the semiperimeter and then passing
        the semiperimeter into Heron's formula
        """
        s = ((self.side_a+self.side_b+self.side_c)/2)
        area = math.sqrt(s*(s - self.side_a)*(s - self.side_b)*(s - self.side_c)
                        )
        return area

    
    def is_right(self):
        """
        Calculates whether a triangle has a right angle
        Args: self- calls upon variables from previous functions
        Returns: (boolean) returns true if one angle = 90 degrees, and false if none of the angles are equal to 90

        """
        self.angle_c = self.law_of_cosines(self.side_a,self.side_b,self.side_c)
        self.angle_b = self.law_of_cosines(self.side_a,self.side_c,self.side_b)
        self.angle_a = self.law_of_cosines(self.side_c,self.side_b,self.side_a)
        if self.angle_a == 90 or self.angle_b == 90 or self.angle_c == 90:
            return True
        else:
            return False


if __name__ == '__main__':
    tri = Triangle(3, 4,5)
    print(tri.is_right())
    print(tri.area)