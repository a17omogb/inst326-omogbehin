""" Funcion Definitions + Arguments: The funciton is_valid takes three integers a,b, and c and returns True if the values are valid for a triangle
The law of cosines uses the same integer arguments in order to calculate some angle in degrees.
The area function takes the same integer arguments as is_valid and calculates the area of the triangle.

Returns: The is_valid function returns true if the triangle sides can be used for a valid triangle.
law_of_cosines raises a value error if the values passed into the function return false for the is_valid function.
Otherwise, it will use the law of cosines formula and return the area of the angle across from side c.

The area function will raise a value error if the arguments return false for the is_valid function.
Otherwise, it uses the area formula for a triangle and returns the caluclated value

The main function will exit the terminal if the is_valid function returns false.
Otherwise, it will use the law_of_cosines function to calculate the three angles of the triangle in degrees, print them, and then also print the area of the triangle in units squared using the area function.
"""""



def is_valid(a,b,c):
    if (a + b) > c:
        if (a + c) > b:
            if (b + c) > a:
                return True
    else:
        return False
    
def law_of_cosines(a,b,c):
    if is_valid(a,b,c)==False:
        raise ValueError
    else:
        import math
        cosine = math.acos((a**2+b**2-c**2)/(2*a*b))
        return (math.degrees(cosine))
    
def area(a,b,c):
    if is_valid(a,b,c)==False:
        raise ValueError
    else:
        import math
        s=((a+b+c)/2)
        area_calc= math.sqrt(s*(s-a)*(s-b)*(s-c))
        return area_calc
    
def main(a,b,c):
    import sys
    if is_valid(a,b,c)==False:
        print("Error: Triangle not valid")
        sys.exit()
    else:
        angle_c= law_of_cosines(a,b,c)
        print('angle c: ',angle_c)
        angle_b= law_of_cosines(a,c,b)
        print('angle b: ',angle_b)
        angle_a= law_of_cosines(b,c,a)
        print('angle a: ',angle_a)
        print('area of triangle: ', area(a,b,c))
        
if __name__=='__main__':
    import sys
    main(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))