""" Find words that are only on the one list"""
from pathlib import Path


class WordListProcessor:
    """A class that can find unique items out of multiple word lists.
    
    Attributes:
        word_lists (dict of str: set of str): the word lists we want to
            evaluate; each key is a name of a wordlist; each value is
            a set of words
    """
    def __init__(self,paths):
        """Define word_lists attribute such that each key is a name of
        a file in paths and each value is a set of words."""
        
        self.word_lists = {}
        for path in paths:
            name = Path(path).stem
            self.word_lists[key] = self.read(path)
    
    def read(self,path):
        """read words from a file and return them as a set.
        
        Args:
            path (str): path to a file containing one word per line.
            
        Returns:
            set of str: a set containing the words from the file.
        """
        words = set()
        with open(path,'r',encoding='utf-8') as f:
            for line in f:
                line = line.strip()
                words.add(line)