"""simulates a STAR voting election"""

import sys



class Ballot:
    
    
    def __init__(self,vote_str):
        
        
        """
        passes vote_str to the object's parse_votes() method and stores the 
        result in the votes attribute.
      
         Args:
        vote_str(str): a string consisting of candidates and scores; 
        candidate/score pairs are separated by commas; each candidate is
        separated from their corresponding score by a colon.
        The string may or may not end in a newline character.
     
        """
        self.votes = self.parse_votes(vote_str)
          
        
    
    def parse_votes(self,vote_str):
        """
        builds a dictionary where the keys are candidate names and the
        values are the scores assigned to those candidates as integers.
        
        Args:
        vote_str(str): a string consisting of candidates and scores; 
      candidate/score pairs are separated by commas; each candidate is
      separated from their corresponding score by a colon.
      The string may or may not end in a newline character.
      
        Returns:
        votes_dict(dict): returns the dictionary in the format
        {candidate1:score1,candidate2:score2,etc.}
        
        """
        votes_dict={}
        for vote in vote_str.split(","):
            for (candidate,score) in vote.split(":"):
                votes_dict[vote.split(":")[0]]=int(vote.split(":")[1])
        return votes_dict
    def preference(self,candidate1,candidate2):
        """
        determines if the ballot shows a preference for either of the two
        candidates passed as arguments. 
        
        Args:
        candidate1(str): a string representing the name of candidate 1
        candidate2(str): a string representing the name of candidate 2
        
        Returns:
        - returns the name of candidate 1 if its total votes is greater than
        candidate 2
        - returns the name of candidate 2 if its total votes is greater than
        candidate 1
        - returns None if anything other than the above is passed through
        
        """
        if self.votes[candidate1] > self.votes[candidate2]:
            return candidate1
        elif self.votes[candidate2] > self.votes[candidate1]:
            return candidate2
        else:
            return None
        
def read_ballots(path):
    """
    opens the specified file for reading and creates instances of Ballot
    for each line in the file.
    
    Args:
    path(str): a path to a text file containing one ballot per line
    
    Returns:
    returns a list of instances of the Ballot class and stores them in
    a variable called ballot_list 
    
    """
    with open(path,'r',encoding='utf-8') as f:
            
        for line in f:
            ballot_list=[]
            line.strip("\n")
            ballot = Ballot(line)
            ballot_list.append(ballot)
    return ballot_list
                    
    
    
def find_winner(ballot_list):
    """
    determines the winner based on the top candidate that was preferred
    on the most ballots.
    
    Args:
    ballot_list(list): a list of instances of the Ballot class
    
    Returns:
    returns the name of whichever two candidates was preferred on the 
    most ballots.
    
    
    """
    max_value=0
    for ballot in ballot_list:
      
        for value in ballot.votes.values():
            
            if  value > max_value:
                max_value=value
                continue
            if value < max_value:
                continue
        ballot.preference() 
    return ballot.preference()     
    
def main(path):
    """
    prints the result of find winner to the console.
    
    Args:
    path(str): a path to a text file containing one ballot per line
    
    Side Effects:
    calls read ballots using path, and uses the list returned as an argument
    for find_winner. the result is then printed to the console.
    
    
    """
    print(find_winner(read_ballots(path)))
    
if __name__=='__main__':
    main(sys.argv[1])
    