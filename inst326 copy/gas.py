"""This class simulates a gas station through the use of funtions"""

class GasStation:

    def __init__(self):
        self.low_octane_tank=2000.0
        self.high_octane_tank=1000.0
        self.money=1000
        self.wholesale_low=1.91
        self.wholesale_high=2.21
        self.retail_low=2.31
        self.retail_med=2.45
        self.retail_high=2.59
        
    
    def buy_gas(self,tank_type):
        """simulates the gas station buying enough gas at wholesale rates to fill one of its tanks
        Args: 
        tank_type, type: string, user enters low or high to represent which tank they will be filling 
        Side Effects: sets low_octane_tank to 2000, and high_octane_tank to 1000"""
        
        if tank_type=='low':
            difference = 2000-self.low_octane_tank
            self.money= self.money - (difference * self.wholesale_low)
            self.low_octane_tank = 2000
        if tank_type=='high':
           
            difference = 2000-self.high_octane_tank
            self.money= self.money - (difference * self.wholesale_high)
            self.high_octane_tank = 1000
            
    def sell_gas(self,gallons_num,octane_level):
        """ simulates the gas station selling gas to a customer at retail rates
        Args: 
        gallons_num; type: float,  the number of gallons of gas to be sold (should be a float between 0.1 and 100.0
        octane_level: type: string, and the octane level of the gas to be sold 
        (should be one of the following strings: "low", "med", "high")
        
        Side Effects: 
        - Decreases the amount of the appropriate tank(s) (low_octane_tank and/or high_octane_tank;
         when a user specifies "med" as their octane level, half of the gas should come from each tank)
        - increases money by the number of gallons sold times the appropriate price 
        (retail_low, retail_mid, or retail_high)
        - if either tank (low_octane_tank or high_octane_tank) now contains less than 100 gallons of gas, 
        buys gas for that tank using the buy_gas() method
         
        """
        if octane_level=='low':
            self.low_octane_tank=self.low_octane_tank-gallons_num
            self.money= self.money+(gallons_num*self.retail_low)
            if self.low_octane_tank < 100:
                GasStation.buy_gas(self,'low')
        if octane_level=='med':
            self.low_octane_tank= self.low_octane_tank - (gallons_num/2)
            self.high_octane_tank= self.high_octane_tank - (gallons_num/2)
            self.money=self.money + (gallons_num*self.retail_med)
            if self.low_octane_tank < 100:
                GasStation.buy_gas(self,'low')
            if self.high_octane_tank < 100:
                GasStation.buy_gas(self,'high')
        if octane_level=='high':
            self.high_octane_tank=self.high_octane_tank-gallons_num
            self.money=self.money + (gallons_num * self.retail_high)
            if self.high_octane_tank < 100:
                GasStation.buy_gas(self,'high')